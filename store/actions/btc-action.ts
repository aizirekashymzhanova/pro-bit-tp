import { getServerSideProps } from "../../components/homePage/HomePage";
import { getBtcPrice } from "../slices/btc-slice";
import { AppDispatch } from "../store";

export const fetchBrcPrice = () => {
  return async (dispatch: AppDispatch) => {
    try {
      const {
        props: { data },
      } = await getServerSideProps();
      dispatch(getBtcPrice(data.bpi));
    } catch (error) {
      console.log(error);
    }
  };
};
