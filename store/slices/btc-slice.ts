import { createSlice } from "@reduxjs/toolkit";
import { IBtc } from "../../types/btc.types";

interface Istate {
  btc: IBtc | null;
}
const initialState: Istate = {
  btc: null,
};
const btcSlice = createSlice({
  name: "btc",
  initialState,
  reducers: {
    getBtcPrice(state, { payload }) {
      state.btc = payload;
    },
  },
});

const { getBtcPrice } = btcSlice.actions;
export { getBtcPrice };
export const btcReducer = btcSlice.reducer;
