import type { NextPage } from "next";
import HomePage from "../components/homePage/HomePage";
import styles from "../styles/Home.module.css";

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <HomePage />
    </div>
  );
};

export default Home;
