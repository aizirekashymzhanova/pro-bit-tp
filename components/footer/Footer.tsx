import styles from "./Footer.module.scss";
const Footer = () => {
  return (
    <footer className={styles.footer}>
      <a href="#">Home</a>
      <a href="#">About</a>
      <a href="#">Contact Us</a>
    </footer>
  );
};

export default Footer;
