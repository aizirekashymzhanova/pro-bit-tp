import React, { useEffect, useState } from "react";
import { fetchBrcPrice } from "../../store/actions/btc-action";
import { useAppSelector, useAppDispatch } from "../../store/store";
import Footer from "../footer/Footer";
import Navbar from "../navbar/Navbar";
import styles from "./HomePage.module.scss";
const HomePage = () => {
  const [inputVal, setInputVal] = useState(1);
  const dispatch = useAppDispatch();
  useEffect(() => {
    // console.log(getServerSideProps());

    dispatch(fetchBrcPrice());
  }, []);
  const { btc } = useAppSelector((state) => state.btc);
  // console.log(2 * parseFloat("16, 679.9162"), inputVal);

  return (
    <>
      <Navbar />
      <div className={styles.main}>
        <input
          type="number"
          className={styles.input}
          value={inputVal}
          onChange={(e) => setInputVal(+e.target.value)}
        />
        <div>
          {btc &&
            Object.values(btc).map((btc) => (
              <div key={btc.code} className={styles.card}>
                <span>{btc.code}</span>
                <span>
                  {+inputVal * parseFloat(btc.rate.replace(/,/g, ""))}
                </span>
              </div>
            ))}
        </div>
      </div>
      <Footer />
    </>
  );
};

export default HomePage;

export async function getServerSideProps() {
  // Fetch data from external API
  const res = await fetch(`https://api.coindesk.com/v1/bpi/currentprice.json`);
  const data = await res.json();

  return { props: { data } };
}
