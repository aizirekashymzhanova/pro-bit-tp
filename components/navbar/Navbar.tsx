import styles from "./Navbar.module.scss";
const Navbar = () => {
  return (
    <nav className={styles.nav}>
      <div className={styles.nav__left}>
        <a className={styles.nav__link} href="#">
          BUY
        </a>
        <a className={styles.nav__link} href="#">
          SELL
        </a>
      </div>
      <img
        src="https://cdn-icons-png.flaticon.com/512/2473/2473354.png"
        alt=""
        width="50"
      />
      <div className={styles.nav__right}>
        <a className={styles.nav__link} href="#">
          Sign In
        </a>
        <a className={styles.nav__link} href="#">
          Sign Up
        </a>
      </div>
    </nav>
  );
};

export default Navbar;
